# Summary

[Introduction](introduction.md)

---

- [Lecture 0: Meta](./lecture-0-meta.md)
- [Kata 1: Bash](./kata-1-bash.md)
- [Kata 2: Linux](./kata-2-linux.md)
- [Kata 3: Ansible](./kata-3-ansible.md)
- [Lecture 4: Virtual Machine Basics](./lecture-4-virtual-machine-basics.md)
- [Kata 5: Libvirt and Vagrant](./kata-5-libvirt-vagrant.md)
- [Lecture 6: Networking Basics](./lecture-6-networking-basics.md)
- [Kata 7: Linux IP](./kata-7-linux-ip.md)
- [Kata 8: Linux DNS](./kata-8-linux-dns.md)
- [Kata 9: Linux TCP](./kata-9-linux-tcp.md)
- [Kata 10: Firewalls (WIP)]()
- [Kata 11: Virtual Private Networks](./kata-11-virtual-private-networks.md)
- [Kata 8: System Services (WIP)]()
- [Kata 10: Secret Management (WIP)]()
- [Kata 12: High-performance computing (WIP)]()
- [Kata 13: NIX](./kata-13-nix.md)
- [Bonus Kata 1: Dotfiles (WIP)]()
- [Project demonstration](./project-demo.md)

[Command reference](./commands.md)
[Fablab Server Hall Infrastructure](./infrastructure.md)
[Hardware](./hardware.md)
