# Kata 11: Virtual Private Networks

## Learning objectives

- Can use WireGuard to set up a virtual private network between two hosts
- _Optional_: can use Ansible to provision multiple machines effectively

## System target summary

Two hosts (either virtualized using Vagrant or on two fablab VMs): a server, and a client should:

- Be up to date and have `wg` and `wg-quick` tools installed
- Have a WireGuard RSA keypair in a secure root-only location, such as `/etc/wireguard`
- Have appropriate WireGuard configuration files
  - Subnet should be `10.13.37.0/24`
  - Server should be listening on port 51820/UDP in the internal fablab LAN
  - Client should connect to the `server:51820` and not have a listening port open
  - Both should have different IP addresses on the WireGuard interface inside the said subnet
- Have `wg-quick` enabled as a Systemd service to start the WireGuard VPN automatically

Also, if you know how to use firewalls e.g. [`ufw`](./commands.md#ufw), both systems should:

- Have a firewall enabled and set up as follows
  - Server should allow incoming connections to 51820/UDP and SSH only
  - Client should allow incoming SSH connections only

## Debriefing

### Virtual Private Network — WireGuard

WireGuard is minimal-design, fast, cross-platform VPN implementation.

![](./networking/network-vpn.drawio.svg)

In the diagram a WireGuard VPN is configured between servers 1 and 2. The blue network is the WireGuard **virtual network**, in which all traffic is virtual and actually goes through the gray network as regular packets. Communication inside the VPN subnet `10.13.37.0/24` (which can be unencrypted, like HTTP) will be encrypted and encapsulated in the UDP communication between the servers in the `10.42.0.0/24` subnet.

Both servers have a network interface called `wg0` which can be seen in `ip a`:

```
3: wg0: <POINTOPOINT,NOARP,UP,LOWER_UP> mtu 1420 qdisc noqueue state UNKNOWN group default qlen 1000
    link/none
    inet 10.13.37.2/24 scope global internal
       valid_lft forever preferred_lft forever
```

> Notice that the interface has no `link/ether` field with a MAC address. This is because it is a Linux [TUN interface](https://en.wikipedia.org/wiki/TUN/TAP) used for [tunnels](https://www.cloudflare.com/learning/network-layer/what-is-tunneling/).

The `wg0` interface also has table entries in the routing table which we can view using `ip r`:

```
10.13.37.0/24 dev wg0 proto kernel scope link src 10.13.37.2
```

In this example both servers are inside the same network segment, but they could just as well be your computer within a private LAN and a public VPN server.

Here is a good explanation of Virtual Private Networks, what they are, and what they are not: <https://wireguard.how/meta/what>.

From [Ubuntu's WireGuard introduction](https://ubuntu.com/server/docs/wireguard-vpn-introduction)

> It helps to think of WireGuard primar[i]ly as a network interface, like any other. It will have the usual attributes, like IP address, CIDR, [but no MAC address], and there will be some routing associated with it. But it also has WireGuard specific attributes, which handle the VPN part of things.

Read the [Wireguard conceptual overview](https://www.wireguard.com/#conceptual-overview)

### Resources

- A short overview of WireGuard and its configuration file and commands: <https://ubuntu.com/server/docs/wireguard-vpn-introduction>
- A rambling tutorial on setting up WireGuard and routing and firewall: <https://www.tangramvision.com/blog/what-they-dont-tell-you-about-setting-up-a-wireguard-vpn>
- A rambling tutorial on automating above task with Ansible (goes into ansible vault which is useful knowledge): <https://www.tangramvision.com/blog/exploring-ansible-via-setting-up-a-wireguard-vpn>
- Different WireGuard VPN topologies and netfilter firewall configuration (advanced): <https://www.procustodibus.com/blog/2021/11/wireguard-nftables/>

## Instructions

Compile a document (markdown or [similar](https://orgmode.org/)) outlining your actions on the servers. Highlight and try to explain interesting information you notice during your ride. Don't include full command output if it doesn't contain anything interesting (you can indicate skipped lines with `...`).

> For an extra point, use [`tshark`](./commands.md#tshark) to capture and record all traffic (except SSH) on both machines sent/recvd during steps 2..N+1. Include the capture in the document.

### Step 1 — SSH

You need two machines which you can SSH into. Your options are twofold:

- Select two fablab virtual servers and inform other students of the ones you are using.
  > Or just remove their SSH keys from the servers' `.ssh/authorized_keys` file. Leave the teachers authorized if you want.
- Spin up two Vagrant VMs on your fablab virtual server or on your own computer.

### Step 2..N — Set up the VPN

Follow [this tutorial](https://www.stavros.io/posts/how-to-configure-wireguard/) until **Accessing your home LAN**, we don't need routing between LAN and WireGuard.

> Note that for you to access the fablab internal netwok through the bastion, a very similar solution to **Accessing your home LAN** has been implemented between the bastion and fablab router.
>
> Also note that we don't need to set up **Forwarding all your traffic through** the WireGuard tunnel. However you can try this if you want. Check `ip route get 1.1.1.1` if you get it working.

Skip the tutorial until **Securing and running on startup** and follow on from there.

### Step N+1 — Test the VPN connection

Set up a [netcat](./commands.md#nc) listener on one server, and connect to it through the tunnel using the addresses in `10.13.37.0/24` from the other server. Next, try the same in the other direction.

### Step N+2 — Provision using Ansible (optional)

Convert the steps you took into an ansible playbook. You can check Fablab's ansible files for inspiration. Here is some stuff for setting up WireGuard on the Raspberry Pi's controlling the fablab 3D printers: [main.yml](https://gitlab.com/otafablab/ansible-deployment/-/blob/master/roles/wireguard/tasks/main.yml), [fablab.conf.j2](https://gitlab.com/otafablab/ansible-deployment/-/blob/master/roles/wireguard/templates/fablab.conf.j2).

## Returning and feedback

Return the compiled document and optional Ansible playbook to [Google classroom](https://classroom.google.com/c/NjAzNDI4MzQ3Mjc4/a/NjA5MDc3OTAxODE5/details).

Finally, please fill the [kata feedback form](https://docs.google.com/forms/d/e/1FAIpQLScuGcebcVyQNcLSoYY2m4HbxXunYRfMpB-6TLIFS7inweXgYg/viewform?usp=sf_link), and then move on to the next kata by pressing right.

## Ansible modules

List of the Ansible modules used in this Kata:

| Module                                                                                       | Description          |
|----------------------------------------------------------------------------------------------|----------------------|
| [`TODO`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/TODO_module.html) | Manages TODO-packages |

## Commands and tools

| Command or tool                        | Summary                            |
|----------------------------------------|------------------------------------|
| [`ssh`](./commands.md#ssh)             | {{#include commands.md:ssh}}       |
| [`umask`](./commands.md#umask)         | {{#include commands.md:umask}}     |
| [`tee`](./commands.md#tee)             | {{#include commands.md:tee}}       |
| [`wg`](./commands.md#wg)               | {{#include commands.md:wg}}        |
| [`wg-quick`](./commands.md#wg-quick)   | {{#include commands.md:wg-quick}}  |
| [`chown`](./commands.md#chown)         | {{#include commands.md:chown}}     |
| [`chmod`](./commands.md#chmod)         | {{#include commands.md:chmod}}     |
| [`systemctl`](./commands.md#systemctl) | {{#include commands.md:systemctl}} |
| [`nc`](./commands.md#nc)               | {{#include commands.md:nc}}        |

## Hints

N/A
