# Kata x: Firewalls

## Learning objectives

- Can configure a firewall on Debian-based hosts using `ufw`
- Can list TCP/UDP connections which are listening on a port or established
- Can trace the route taken by packets across routers
- Can use SSH port forwarding to bypass firewalls

## System target summary

TODO

## Debriefing

TODO advanced socat

### Local firewall configuration

TODO ufw, ss
