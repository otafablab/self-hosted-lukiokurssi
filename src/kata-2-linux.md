# Kata 2: Debian Linux

## Learning objectives

- Can configure an SSH server and keypairs securely.
- Can run remote commands automatically using SSH.
- Knows the most common paths in the filesystem hierarchy standard (FHS).
- Can install packages on Debian-based operating systems through APT.
- Can set up automatic updates on Debian-based Linux.

## System target summary

- The Debian host should be updated to the latest version.
- The [Fish shell](https://fishshell.com/) should be installed on the host.
- The user should have the Fish shell set as the default shell.
- Automatic updates should be enabled on the server.

Finally a shell script to do these steps automatically should be created, which runs on the local controller machine and connect to the server to run the commands automatically. The output of the script could be:

```console
$ ./kata2.sh localadmin@1.3.3.7
[+] Connecting to localadmin@1.3.3.7
[+] Updating system
[+] Installing Fish
[+] Setting Fish as the sell for localadmin
[+] Installing unattended-upgrades
[+] Provisioning successful
```

The script should not block and wait user input at any time.

## Debriefing

In this kata you will use the Fablab Virtual Servers (FVS) to learn basic linux usage. The servers are equipped with a [Debian](https://www.debian.org/)-based operating system. You will use SSH to remotely update the server, and then install and configure the Fish shell as well as automatic updates.

### Fablab Virtual Servers

The fablab infrastructure consists of several physical desktop computers (previously e-waste) all with 16 or 32 GiB of RAM. They are topologically located in a separate network segment, the Fablab Internal Network, whose IP subnet is 10.42.0.0/24.

The physical machines have static addresses according to the following table:

| Hostname | IP Address |
|----------|------------|
| rtr      | 10.42.0.1  |
| a1       | 10.42.0.2  |
| a2       | 10.42.0.3  |
| a3       | 10.42.0.4  |
| a4       | 10.42.0.5  |
| b1       | 10.42.0.6  |
| b2       | 10.42.0.7  |
| b3       | 10.42.0.8  |
| b4       | 10.42.0.9  |

> Each machine and its hardware configuration can be found [here](./hardware.md).

Virtual machines running on these desktops are also available within the subnet with dynamic addresses (DHCP). The dynamic IP range is 10.42.0.64 .. 10.42.0.254, so each Fablab Virtual Server has an IP address in that range.

Connecting to a Fablab Virtual Server is not straightforward as they are in their own network segment and not [bridged](https://en.wikipedia.org/wiki/Network_bridge) with the school network. We have three options:

1. Connect to the Fablab Internal Network using an Ethernet cable (requires physical access to the switch).
1. Use the router as an SSH jump host.
1. Create an SSH tunnel to access the whole Fablab Internal Network through the router (advanced). This requires a lot of manual configuration or a tool that does it automatically [sshuttle](https://github.com/sshuttle/sshuttle).

### Linux and the Filesystem Hierarchy Standard (FHS)

Linux is a _free_ and _open-source_ operating system based on the Unix operating system philosophy. It was created by Linus Torvalds in 1991 and has since become the most popular operating systems in the world. It is known for its stability, security, and configurability.

The [Filesystem Hierarchy Standard (FHS)](https://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard) is a set of guidelines that defines the _directory structure_ of Linux-based operating systems.

The FHS defines the roles and responsibilities of various directories in the file system, such as the root directory `/`, `/bin`, `/etc`, and others.

| Directory | Purpose                                                                                                                                                |
|-----------|--------------------------------------------------------------------------------------------------------------------------------------------------------|
| `/`       | Primary root of the hierarchy and top-most directory of the entire file system hierarchy.                                                              |
| `/bin`    | Essential command binaries, such as `cat`, `ls`, `cp`.                                                                                                 |
| `/etc`    | System-wide configuration files.                                                                                                                       |
| `/home`   | Users' home directories, containing saved files, personal settings, etc.                                                                               |
| `/var`    | Variable files: files whose content is expected to continually change during normal operation of the system, such as logs.                             |
| `/usr`    | Read-only user data: contains the majority of multi-user utilities and applications                                                                    |
| `/boot`   | Boot loader files (e.g., [kernels](https://en.wikipedia.org/wiki/Kernel_(operating_system)), [initrd](https://en.wikipedia.org/wiki/Initial_ramdisk)). |

In Linux, user management is implemented through the use of user accounts. Each user account has a unique username (for example `localadmin`) and a corresponding user ID (for example `1000`) that are used to identify the user.

There is a user which has access to everything on the system, known as the `root` user. Its user ID is always set to `0` and it is available on all Linux systems. Access to this user account is often done via the `sudo` command.

The philosophy behind Linux user management is based on the **principle of least privilege**. This means that users are granted only the permissions necessary to perform their specific tasks, and nothing more. This approach helps to prevent accidental or malicious damage to the system.

### [sudo](https://en.wikipedia.org/wiki/Sudo)

The [`sudo`](./commands.md#sudo) command in Linux is used to run commands with elevated privileges, which typically means the root user.

`sudo` provides a way for users to perform tasks that require administrative access without logging in as the `root` user (`root` login via password is also often disabled). For example, installing new software, modifying system configurations, or performing system maintenance tasks.

The configuration file for which users and groups may use `sudo` is located in `/etc/sudoers` on most systems.

If you want to login as `root` on a system where you are authorized to use `sudo`, you can run

```shell
sudo su
```

### Secure Shell (SSH)

Secure Shell, or SSH, is a tool used to remotely manage a server through a shell. The server host needs to have an SSH server installed, running and open to the network. Authenticating to the server can be done using a password or a [private key](https://www.ssh.com/academy/ssh-keys) depending on the configuration.

When logging in to a server using SSH for the first time, the following warning should appear:

```text
The authenticity of host '1.3.3.7 (1.3.3.7)' can't be established.
ED25519 key fingerprint is SHA256:4mkW0O0aIgenHjSZoNGAgWwuhiunBxyQBsOtuLNwyMo.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])? 
```

This warning tells you that there is no certainty whether you are connecting to the correct machine or some malicious server. This is because the internet protocol has no built-in security and anyone can claim any IP address within a network. This is especially problematic when connecting to any public server on the internet.

**Do not ignore this warning** unless your computer and the server are connected through a **trusted network** (such as a properly set up VPN). Similarly as HTTPS, SSH is encrypted, but unlike HTTPS, SSH does not use the [X.509](https://en.wikipedia.org/wiki/X.509) certificate chain system but rather requires manual verification of the _host keys_.

The secure way to establish first contact with the server is to verify the _host key fingerprint_ of the system securely. You can read the fingerprint from the warning message. This can be via the server's sysadmin or logging in to the machine locally and displaying the fingerprint there.

Read more:

- [SSH keypairs](https://www.ssh.com/academy/ssh/keygen)
- [SSH host keys](https://www.ssh.com/blog/what-are-ssh-host-keys).
- Advanced information about [SSH certificates](https://berndbausch.medium.com/ssh-certificates-a45bdcdfac39)

## Instructions

### Step 1 — SSH

Connect to your virtual "private" server using SSH:

- Open a terminal on the client machine.
- Enter the following command: `ssh localadmin@IP`, with `IP` replaced with your Fablab Virtual Server's IP address.
- Compare the fingerprint to the one provided to you by the sysadmin.
- If they match (are equal) type `yes` and press `RETURN`.

### Step 2 — Update the server

Enter the following commands to update and upgrade the system: 

```shell
sudo apt update
sudo apt upgrade
```

> On the fablab virtual machines, the user can use sudo without a password.

### Step 3 — Install the Fish shell

Enter the following command to install the Fish shell:

```shell
sudo apt install fish
```

### Step 4 — Set the Fish shell as the default

Enter the following command to set the Fish shell as the default shell:

```shell
sudo chsh -s /usr/bin/fish localadmin
```

### Step 5 — Automatic updates

Search online for how to install and configure `unattended-upgrades`. For example the following resource may be useful:

- <https://linuxiac.com/how-to-set-up-automatic-updates-on-debian/>

You don't need to add any special configuration, but if you want, configure the service to send email to you. This command should show that the service is active:

```shell
sudo systemctl status unattended-upgrades.service
```

### Step 6 — Log out

Log out by entering the `exit` command, or by pressing [`CTRL-D`](./commands.md#command-line-shortcuts).
  
### Step 7 — Shell script

Convert the previous steps into a shell script, which does _remote provisioning_ i.e. runs commands on a remote system.

The shell script should be on the _controller_ machine (your computer) and should take the username and IP address of the target host as arguments. The script should use SSH to run the provisioning commands on the remote machine.

> Search the internet for how to pass arguments to Bash scripts

Finally, the script should not block and wait user input, so you need to run the `apt` commands with the `-y` flag.

## Returning

Return the final Bash script from Step 7 to the [Google Classroom assignment](https://classroom.google.com/c/NjAzNDI4MzQ3Mjc4/a/NjAzNDk1MDk2MjY4/details):

Finally, please fill the [kata feedback form](https://docs.google.com/forms/d/e/1FAIpQLScuGcebcVyQNcLSoYY2m4HbxXunYRfMpB-6TLIFS7inweXgYg/viewform?usp=sf_link), and then move on to the next kata by pressing right.

## Commands and tools

| Command or tool              | Summary                       |
|------------------------------|-------------------------------|
| [`ip`](./commands.md#ip)     | {{#include commands.md:ip}}   |
| [`ssh`](./commands.md#ssh)   | {{#include commands.md:ssh}}  |
| [`sudo`](./commands.md#sudo) | {{#include commands.md:sudo}} |
| [`apt`](./commands.md#apt)   | {{#include commands.md:apt}}  |
| [`vim`](./commands.md#vim)   | {{#include commands.md:vim}}  |
| [`chsh`](./commands.md#chsh) | {{#include commands.md:chsh}} |
| [`fish`](./commands.md#fish) | {{#include commands.md:fish}} |

## Hints

- If you get stuck in SSH due to a connection problem for example, enter the following 3 characters: `RETURN ~ .`
