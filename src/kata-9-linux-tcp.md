# Kata 9: Linux TCP

## Learning objectives

- Can work with plain TCP sockets: `nc`, `socat`
- Can send HTTP requests using `curl`
- Can open a TCP socket and pipe it to a file
- Can open a file and pipe it to a TCP socket
- Can compress and decompress Tape Archive Records automatically when using `nc`
- Can download files using `wget`

## Debriefing

### Linux sockets, netcat

On Linux, a _socket_ is used to identify how processes "talk to the network". A socket is an interface provided by the kernel used by applications to send data over TCP for example.

Without the socket abstration, applications would need to manually implement the TCP/IP stack and write data (bits) directly to the network interface. The purpose of an operating system is to allow a more unified and controlled access to hardware (and software) resources.

The kernel controls which kind of packets non-root users are allowed to send and what ports they are allowed to listen on. On linux, a user can't send arbitrary TCP or UDP or IP packets, and can't listen on ports below 1024 by default.

Netcat, or [`nc`](./commands.md#nc), is a bare-bones TCP and UDP client for communicating with servers on the network.

- To connect to an HTTP server running at 1.3.3.7, run
  ```
  nc -nv 1.3.3.7 80
  ```
- To listen on port 1234 and print out received data, run
  ```
  nc -nvlkp 1234
  ```

> If you want arrow keys and history to work within `nc`, use [`rlwrap`](./commands.md#rlwrap)

### Shell redirections

Each process you launch from the command line has an input stream, called _standard input_ and an output stream called _standard output_. When typing commands into bash, you are typing them to the bash process's standard input stream, from which bash can read them and act accordingly printing the output to the standard output stream.

We can override the stdin and stdout streams of processes launched from bash using `<` (input), `>` (output) and `|` (output -> input). 

For example we can redirect the standard output from echo to a file `echo kana > file`. We can then try to run the file as a python script with `python3 < file` (notice that in the output the filename is shown as `<stdin>`). We can even use `|` to do both of these steps without creating the intermediate `file`: `echo kana |python3`

> A common mistake is to read and write the same file in the same command. Guess [what happens](https://stackoverflow.com/questions/3887771/in-unix-what-cat-file1-file1-does)?

For more details, read this Redhat's [guide on redirections](https://www.redhat.com/sysadmin/redirect-operators-bash).

### Downloading files from the World Wide Web

Files and software are commonly downloaded from the Internet using tools such as [`wget`](./commands.md#wget) or [`curl`](./commands.md#curl). When using an `http://` url the connection is unencrypted and the file could be tampered with. When downloading software, always use `https://` urls, which in addition to encryption, checks authenticity of the server. If available, check the [checksum](https://en.wikipedia.org/wiki/Checksum) provided by the vendor.

## Instructions

Compose a document (text, markdown) which captures the relevant information shown in the commands that you run.

> For an extra point, use [`tshark`](./commands.md#tshark) to capture and record all traffic (except SSH) sent/recvd during steps 2 – 6. Include the capture in the document.

### Step 1 — SSH sessions

In two terminals open SSH connections to two different fablab virtual servers; we'll refer to them as server 1 and 2 from now on. Record the IP addresses you used in your document.

### Step 2 — Plain TCP chat using nc

One server 1, run `nc -nvlkp 1234` to listen for TCP connections to port 1234 of that server.

> Notice that because Ubuntu servers don't have a firewall enabled by default, this service is insecurely available to all hosts in the fablab LAN.

> You can press `Ctrl-Z` to pause the application and get a shell prompt. Now, running `ss -tlpn` shows `nc` listening at `0.0.0.0:1234`. Return to and resume the netcat with the `fg` command.

On server 2, run `nc -v 1.3.3.7 1234` with the IP address (or DNS hostname) of server 1. This should connect the sockets and establish a two-way connection. Write any messages you wish into both netcats and record the outputs into your document.

### Step 3 — HTTP 

1. Send the following [HTTP 1.1 request](https://en.wikipedia.org/wiki/HTTP#HTTP/1.1_request_messages) to `example.com` port 80 over TCP with `nc`.
   ```text
   GET / HTTP/1.1
   host: example.com
   ```

   > In the HTTP/1.1 protocol, all header fields except Host: hostname are optional.

1. Connect to `example.com` using `curl`.
   ```shell
   curl http://example.com
   ```

1. Keep the netcat server running on server 1 and using `curl` connect to it from server 2.
   ```shell
   curl http://1.3.3.7:1234
   ```
   You can exit the `curl` or shut down the netcat server with `Ctrl-C`.

### Step 4 — TCP file transfer

Let's transfer a file from server 2 to server 1 using netcat. First, we need to create a listener on server 1 which redirects the received bytes into a file. To do this, run

```shell
nc -nvlp 1234 > file
```

This server doesn't have the `-k` (keep open) flag which means it will shut down when the client finishes. The standard output is redirected with `> file` creating it if it doesn't exist and overwriting it otherwise.

Now let's send `/etc/hostname` from server 2.

```shell
nc -vq0 1.3.3.7 1234 < /etc/hostname
```

On server 1, the listener should have exited, and there should be a `file` that we can read.

```shell
cat file
```

### Step 5 — Large file transfer with compression

1. On server 2, create a 1GiB file using [`dd`](./commands.md#dd).
   ```shell
   dd if=/dev/zero of=data bs=1MiB count=1024
   ```
1. Append some text at the end with [`echo`](./commands.md#echo).
   ```shell
   echo kana >> data
   ```
1. Dump the content of the file using [`hexdump`](./commands.md#hexdump)
   ```shell
   hexdump -C data
   ```
1. Start the netcat on server 1 with a pipe to [`tar`](./commands.md#tar) in order to e`x`tract the data.
   ```shell
   nc -nvlp 1234 |tar xvzf -
   ```
1. `c`ompress the data and toss it over from server 2.
   ```shell
   tar cvzf - data |nc -vq0 1.3.3.7 1234
   ```
1. Inspect the data on server 1
   ```shell
   hexdump -C data
   ```

### Step 6 — Downloading files from HTTP servers

We could use netcat or curl to download files from the Internet over HTTP, but there is a tool dedicated for this: [`wget`](./commands.md#wget).

> Note: run the following commands on your laptop as the Internet speed is probably much better than on the Fablab servers.

Download the Linux source code (~200MiB) from GitHub using the following command.

```shell
wget "https://github.com/torvalds/linux/archive/refs/tags/v6.3.tar.gz"
```

Once it's finished, unarchive the gzipped tape archive record.

```shell
tar xzf v6.3.tar.gz
```

Finally check that the Rust samples are present.

```shell
cat linux-6.3/samples/rust/rust_minimal.rs
```

## Returning and feedback

Return the compiled document to [Google classroom](https://classroom.google.com/c/NjAzNDI4MzQ3Mjc4/a/NjA4MTE4NzI5OTU1/details).

Finally, please fill the [kata feedback form](https://docs.google.com/forms/d/e/1FAIpQLScuGcebcVyQNcLSoYY2m4HbxXunYRfMpB-6TLIFS7inweXgYg/viewform?usp=sf_link), and then move on to the next kata by pressing right.

## Commands and tools

| Command or tool                    | Summary                          |
|------------------------------------|----------------------------------|
| [`ssh`](./commands.md#ssh)         | {{#include commands.md:ssh}}     |
| [`nc`](./commands.md#nc)           | {{#include commands.md:nc}}      |
| [`rlwrap`](./commands.md#rlwrap)   | {{#include commands.md:rlwrap}}  |
| [`curl`](./commands.md#curl)       | {{#include commands.md:curl}}    |
| [`dd`](./commands.md#dd)           | {{#include commands.md:dd}}      |
| [`echo`](./commands.md#echo)       | {{#include commands.md:echo}}    |
| [`cat`](./commands.md#cat)         | {{#include commands.md:cat}}     |
| [`hexdump`](./commands.md#hexdump) | {{#include commands.md:hexdump}} |
| [`tar`](./commands.md#tar)         | {{#include commands.md:tar}}     |
| [`cat`](./commands.md#cat)         | {{#include commands.md:cat}}     |
| [`wget`](./commands.md#wget)       | {{#include commands.md:wget}}    |

## Hints

N/A
