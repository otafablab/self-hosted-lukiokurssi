# Bonus Kata 2: Virt-manager

## Learning objectives

- Can use `virsh` and `virt-manager` to manage libvirt objects.

## System target summary

TODO

## Instructions

## `virt-manager` and `virsh`

There are a couple more tools that are useful when working with libvirt. One is `virt-manager`, a graphical user interface for managing libvirt virtual machines. Now that you have a VM running, we can see it in `virt-manager` after some port forwarding magic.

Install the `virt-manager` bloatware on your computer first. Next, run the following command substituting the IP with your Fablab virtual server's.

```console
ssh jump@bastion.fablab.rip -N -L localhost:2222:1.3.3.7:22
```

> This SSH command sets up local port forwarding from port 2222 to the Fablab virtual server's port 22. [Read more](https://iximiuz.com/en/posts/ssh-tunnels/)

Now make sure the SSH connection works (this also adds the host to known hosts file).

```console
ssh localadmin@localhost -p 2222
```

Finally, open `virt-manager` with the following connection URI.

```console
virt-manager -c 'qemu+ssh://localadmin@localhost:2222/system'
```

This should open a graphical window where you can inspect and modify the VMs. To inspect a VM, double click on it and it opens another window.

Notice that there are two tabs in the viewer window that just opened: Graphical console (screen picture) and Hardware details (info picture). The Hardware details contains all the interesting details about the VM. For example the last entry is probably the shared directory.

> If you encounter the following error
> 
> ```
> Viewer was disconnected.
> SSH tunnel error output: ssh_askpass: exec(/usr/libexec/openssh/ssh-askpass): No such file or directory
> Host key verification failed.
> ```
> 
> Try using `virt-viewer` directly to connect to the graphical console.
> 
> ```console
> virt-viewer -c 'qemu+ssh://localadmin@localhost:2222/system'
> ```

TODO virsh remote connection

## Commands and tools

| Command or tool                              | Summary                               |
|----------------------------------------------|---------------------------------------|
| [`virt-manager`](./commands.md#virt-manager) | {{#include commands.md:virt-manager}} |
| [`virt-viewer`](./commands.md#virt-viewer)   | {{#include commands.md:virt-viewer}}  |
| [`virsh`](./commands.md#virsh)               | {{#include commands.md:virsh}}        |
