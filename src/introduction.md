# Self-hosted Server Infrastructure Course

## General information

Are you tired of the tech oligopoly, a.k.a. the _big brother_, hosting all the services you use daily? Have you looked for a solution which doesn't involve sharing your personal information with a third-party? Do you believe in _freedom_?

If so, then this course is for you.

You will learn the fundamentals of creating your own _private cloud_ (often called a _home lab_ when it's in your home), including system administration of the GNU/Linux operating system, provisioning systems, automatic deployment of software, networking, secret management and more.

This course was created by Niklas Halonen with the help of Matias Zwinger.

> NOTE: This course is still a draft and any feedback is greatly appreciated. Especially any feedback regarding:
> - The exercise instructions and their clarity
> - How easy or hard it was to find some information
> - Feelings about being lost (if you are truly lost, contact the teacher)

## What is a home lab

A home lab is a sandbox environment to host private services and practice system administration. For example, a home lab can constist of

- network attached storage (NAS) drives for backups, photos etc.
- virtual private servers (VPS) for running the operating system (typically GNU/Linux) under the services
- virtual private networks (which typically require an external bastion host)
- private password manager server
- home automation server
- game servers, such as a Factorio server

Usually a home lab is made up from old and slow computer hardware (which might even be considered electric waste by some corporations (not naming any names here though)). However, the computers typically use new disk drives and SSDs in order to have more storage space and avoid potential data loss upon inevitable failure for longer.

> NAS hard-drives can be quite expensive, which is a big upfront cost for newbies getting into home lab server hosting. Do not buy cheap hard-drives if you are planning to keep important backups on them! Do your own research about the topic.

Most of all, a home lab is a safe zone to fail and learn while still retaining your data and privacy (and showing a middle-finger to the Big Tech). Creating and maintaining a home lab is just the first step on a career towards becoming a highly sought after system administrator.

## Learning objectives

- Knows basic administration skills
  - Can securely connect to Linux servers
	- Can use SSH
  - Can update Linux servers
	- Knows how to setup automatic updates
  - Knows how to make changes on Linux servers
	- Can use Bash and Ansible
  - Knows common Linux services and how to configure them
  - Understands the security risks of managing a server connected to the internet
- Knows how virtual machines operate
  - TODO vagrant, libvirt
- Knows basics of IP networking
  - Understands basics of internet security
	- Can setup firewalls and monitor security events
  - Knows what TLS certificates are and how to use them
- Has hands-on experience on provisioning and deploying a Linux service
