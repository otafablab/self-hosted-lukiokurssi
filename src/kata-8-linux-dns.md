# Kata 8: Linux DNS
## Learning objectives

- Can read the DNS client settings with `ip` and `resolvectl`
- Can query DNS servers for `A` records with `nslookup` and `dig`

## System target summary

No changes required. Read the [instructions](#instructions).

## Debriefing

Your task is to gather information about a server you have shell access to, e.g. one of your fablab virtual servers.

### Domain name system

The [Domain Name System (DNS)](https://en.wikipedia.org/wiki/Domain_Name_System) is used to find the IP address of a human readable _domain name_, such as `bastion.fablab.rip`, on the Internet.

DNS is an arcane unauthenticated (provides no trust), unencrypted (provides no privacy) protocol that has absolutely no right to exist anymore but still it's used by nearly all computers on the Internet today. Read this [Cloudflare's article](https://www.cloudflare.com/learning/dns/dns-over-tls/).

> DNS also only supports ASCII letters (a-z), so international domain names (unicode) are converted through the [IDN](https://en.wikipedia.org/wiki/Internationalized_domain_name) system.

To get the address of the DNS server used by your Linux operating system, you can read the [`/etc/resolv.conf`](https://www.man7.org/linux/man-pages/man5/resolv.conf.5.html) file.

```shell
$ cat /etc/resolv.conf
# This is /run/systemd/resolve/stub-resolv.conf managed by man:systemd-resolved(8).
# Do not edit.

nameserver 127.0.0.53
options edns0 trust-ad
search .
```

The important line is the `nameserver 127.0.0.53` which means that you are using a DNS server running on your host (loopback network is the whole `127.x.y.z/8`). Because the kernel only provides a simple system for DNS management, a system service called [`systemd-resolved`](https://wiki.archlinux.org/title/Systemd-resolved) is used on many Linux distributions to manage DNS settings. To get the DNS servers used by `systemd-resolved`, run `resolvectl`.

```shell
$ resolvectl
Global
       Protocols: -LLMNR -mDNS -DNSOverTLS DNSSEC=no/unsupported
resolv.conf mode: stub

Link 2 (ens3)
    Current Scopes: DNS
         Protocols: +DefaultRoute +LLMNR -mDNS -DNSOverTLS DNSSEC=no/unsupported
Current DNS Server: 8.8.8.8
       DNS Servers: 8.8.8.8 fe80::213:37ff:fea6:f434%21985

Link 3 (virbr0)
Current Scopes: none
     Protocols: -DefaultRoute +LLMNR -mDNS -DNSOverTLS DNSSEC=no/unsupported
...
```

As we can see, the actual DNS server used is `8.8.8.8` which is Google's primary DNS server (`dns.google`).

> I spent 30mins trying to set up DNS over TLS on Ubuntu but couldn't do it. You are free to try it yourself.

We can use [`nslookup`](./commands.md#nslookup) to query the DNS server for a hostname.

```shell
$ nslookup gnu.org
Server:         127.0.0.53
Address:        127.0.0.53#53

Non-authoritative answer:
Name:   gnu.org
Address: 209.51.188.116
Name:   gnu.org
Address: 2001:470:142:5::116
```

As we can see `gnu.org` points to `209.51.188.116`. There is also an IPv6 address available `2001:470:142:5::116` for the same DNS hostname.

Using [`dig`](./commands.md#dig) we can get more information.

```shell
$ dig gnu.org ANY
; <<>> DiG 9.18.12-0ubuntu0.22.04.1-Ubuntu <<>> gnu.org ANY
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 47965
;; flags: qr rd ra; QUERY: 1, ANSWER: 10, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 65494
;; QUESTION SECTION:
;gnu.org.                       IN      ANY

;; ANSWER SECTION:
gnu.org.                300     IN      SOA     ns1.gnu.org. hostmaster.gnu.org. 2023032509 3600 120 1209600 3600
gnu.org.                300     IN      CAA     0 issue "letsencrypt.org"
gnu.org.                300     IN      NS      ns1.gnu.org.
gnu.org.                300     IN      NS      ns3.gnu.org.
gnu.org.                300     IN      NS      ns4.gnu.org.
gnu.org.                300     IN      TXT     "v=spf1 ip4:209.51.188.0/24 ip4:74.94.156.208/28 ip6:2001:470:142::/48 ip6:2603:3005:71a:2e00::/64 ~all"
gnu.org.                300     IN      MX      10 eggs.gnu.org.
gnu.org.                300     IN      AAAA    2001:470:142:5::116
gnu.org.                300     IN      A       209.51.188.116
gnu.org.                300     IN      SSHFP   1 1 A2B0FA94793B921FC7A835A313CE8557F8D989E1
```

From the output we can see that `gnu.org` resolves to the `209.51.188.116` IPv4 address as indicated by the `A` record. The `AAAA` record usually tells the IPv6 address of the same server.

More resources:

- <https://wiki.archlinux.org/title/Domain_name_resolution>

## Instructions

Compose a document (text, markdown) containing the following details:

- Actual DNS server addresses on all interfaces of the fablab virtual server
- The `A` record of `fablab.rip` and `espoo.fi`
  > You can query these on your machine too, or use the fablab virtual server
- The [authoritative nameservers](https://www.cloudflare.com/learning/dns/dns-server-types/#authoritative-nameserver) `NS` records used for those domains.

And the commands used to obtain said details.

> You can use the commands shown in the debriefing.

## Returning and feedback

Return the document to the [Google classroom](https://classroom.google.com/c/NjAzNDI4MzQ3Mjc4/a/NjA3OTg5MDYyNjYw/details).

Finally, please fill the [kata feedback form](https://docs.google.com/forms/d/e/1FAIpQLScuGcebcVyQNcLSoYY2m4HbxXunYRfMpB-6TLIFS7inweXgYg/viewform?usp=sf_link), and then move on to the next kata by pressing right.

## Commands and tools

| Command or tool                          | Summary                             |
|------------------------------------------|-------------------------------------|
| [`ssh`](./commands.md#ssh)               | {{#include commands.md:ssh}}        |
| [`ip`](./commands.md#ip)                 | {{#include commands.md:ip}}         |
| [`resolvectl`](./commands.md#resolvectl) | {{#include commands.md:resolvectl}} |
| [`nslookup`](./commands.md#nslookup)     | {{#include commands.md:nslookup}}   |
| [`dig`](./commands.md#dig)               | {{#include commands.md:dig}}        |

## Hints

N/A
