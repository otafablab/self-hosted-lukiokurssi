# Kata 0: Meta

## Learning objectives

- Knows what this course is about.
- Knows what _katas_ are.
- Can interpret and approach katas on this course.
- Knows how to return the exercises for grading

## Introduction

This course consists of a set of exercises, which we will refer to as _katas_. I have tried to keep the katas independent from each other, so you can do the ones you want in the order you want; but do note that the skills and tools learned from prior katas are used in the later ones.

The set of exercises will teach you core skills and tools in _DevOps_, system administration, software development and information security. To be clear, each skill and tool is useful in all of these disciplines.

DevOps is a vast landscape of tools and practices which help development and IT operations to work like [lean manufacturing](https://en.wikipedia.org/wiki/Lean_manufacturing). The key to DevOps is perhaps [continuous improvement](https://en.wikipedia.org/wiki/Continual_improvement_process) which this course is very much about, so feel free to call this course a DevOps course.

Some of the technical aspects of DevOps include:
- Deployment automation
- Infrastructure as code
- Continuous integration and delivery
- Monitoring and observability
- Integrating security throughout the whole process

### What are _katas_?

[Katas](https://en.wikipedia.org/wiki/Kata) are exercises or challenges designed to improve skills through hands-on training. In Japanese martial arts practice, katas are choreographed movements which are repeated and practiced alone. The word in Japanese stands for _form_ or _pattern_. It has been adopted by programmers (who sometimes think of themselves as martial artists of code) to mean a short hands-on exercise.

Not everything can be taught as katas, and therefore we also have _lectures_ which contain material used in the related katas.

### Structure of a kata

The katas on this course are mostly structured as follows:

- A set of **learning objectives** is introduced
- A **system target summary** is given to give the student a sense of what they are supposed to achieve
- A **debriefing** is given to explain how the student can (and is supposed to) engineer the system toward the target
- More detailed **instructions** follow, which might be necessary for the less advanced students
- Instructions for **returning** the work
- **Commands and tools** will be summarized at the end of the kata, and will serve as a reference to more resources
- **Hints** are the final subsection, and might help you if you get stuck
  > Note: the hints may be lacking during the draft rundown of the course

## Returning

Exercise work and proof of completion will be returned to the Google Classroom per kata (I know, using Google's services for this is somewhat distasteful). Each kata has a section on which files to return respectively.

### Feedback

Each kata can be graded and feedback can be given using this [Google Sheets form](https://docs.google.com/forms/d/e/1FAIpQLScuGcebcVyQNcLSoYY2m4HbxXunYRfMpB-6TLIFS7inweXgYg/viewform?usp=sf_link).

## Final warning

You are about to embark on a wild journey of learning various mystical, trivial, ancient and contemporary tools with just you, your computer and the internet with you. So I must warn you, that the journey is not easy, nor does it have an end. Some of these tools have been around for decades, some will be used decades later, but the core problems these tools solve, the core concepts of software engineering, will stay the same (probably). Unfortunately to learn to think like an engineer you need to learn to use the tools too, however complicated, boring, inelegant, frustrating the tools are to use.

But that is what you have signed up for, to learn inelegant tools to master an incredible technological skill, right?

So, having heeded my warning, please proceed to the chapter, where you will be introduced to the infamous, dreaded, hideous tool loved by no one, [Bash](./kata-1-bash.md).
