# Self-Hosted Project Demonstration

## Learning objectives

- Can find open source projects online
- Can find and research the documentation of said project
- Can set up said project running on a server
- Knows how to maintain the host running said project
- Can demonstrate learned skills using a presentation for example

## Debriefing

Your task is to find an open source project you are interested in and set it up on the fablab infrastructure or your own. 

> The ideal outcome is completely automated installation and configuration (with e.g. Ansible), but this is not required.

You will find Vagrant a highly useful tool for quickly setting up a new environment where to test setting up said project and automating it.

You should also prepare a short (10min) demonstration about the project. The demonstration could include

- Running ansible scripts
- Showing how to use the software you self-hosted
- A short presentation depicting how you approached the project

## Example projects to self-host

### PiHole

[Pihole](https://pi-hole.net/) is a easy-to-setup self-hosted DNS server which provides network-wide advertisement/content blocking.

### Bitwarden

[Bitwarden](https://bitwarden.com/open-source/) is a convenient password manager which is easy to host by yourself. The self-hosted version of bitwarden has all the premium features.

> There is also an unofficial [Rust fork](https://github.com/dani-garcia/vaultwarden) available.

If you choose this project, don't store anything sensitive in there, or on fablab servers in general.

### OPNSense

[OPNSense](https://opnsense.org/) is a router operating system based on FreeBSD. This project requires a dedicated server which you can ask for from the teachers.

Creating a router typically requires multiple network interface controllers, but you can also configure routing between virtual networks (such as WireGuard) and the physical LAN network.

### `sr.ht`

SourceHut is a git server (among serving other things) which you can self-host. It is quite a pain to set up so this project is for experienced nixers.

The publicly hosted service is available at [sr.ht](https://sr.ht/).

### Gitea, GitLab

[Gitea](https://docs.gitea.io/en-us/) and [GitLab](https://docs.gitlab.com/ee/install/#) are self-hostable git servers.

- "Gitea is a painless self-hosted all-in-one software development service"

- "You can install GitLab on most GNU/Linux distributions, on several cloud providers, and in [Kubernetes](https://kubernetes.io/) clusters"

GitLab is the more widely used self-hosted git server, so pick that if you want an easy ride.

### NixOS

Set up [NixOS](https://nixos.org/) on your computer or on Fablab hardware. There is also a [kata  about Nix](./kata-13-nix.md).

### NGiNX

Set up a simple HTTP server or load balancer using [NGiNX](https://nginx.org/en/). Doesn't need to be fancy.

### No project?

If none of the other projects suit you, you can create a presentation about what you learned during the course.
