# Kata 5: Libvirt and Vagrant

## Learning objectives

- Can create VMs manually using Vagrant and libvirt.
- Knows the differences between and use cases of Vagrant and libvirt.

## System target summary

VM server and the VM should be in the following state:
- The VM server:
  - Should be updated to the latest version.
  - Should have libvirt and Vagrant installed.
  - Should have a Vagrant managed Ubuntu 22.04 VM running and accessible through SSH locally.
- The VM:
  - Should have 1GiB of memory.
  - Should have the project folder (`./`) available at `/vagrant`.
  - May have the user `fablab` and `fish` and `neofetch` installed as described in [kata 2](./kata-2-linux.md).

## Debriefing

### Vagrant

[Vagrant](https://www.vagrantup.com/) is a tool for creating and managing virtual machines. Vagrant itself is not a virtual machine hypervisor. It supports different hypervisors, like QEMU/KVM or [VirtualBox](https://www.virtualbox.org/) (which is the default). 

Vagrant streamlines setting up custom local virtual machines for development use. It is really helpful when planning/testing remote system provisioning (using Ansible for example). Vagrant can also be used to easily set up Ubuntu, Debian and Fedora VMs to try out or to develop software on.

The virtual machine images, known as Vagrant boxes, can be downloaded and updated from the internet. You can search for the available boxes from [app.vagrantup.com](https://app.vagrantup.com/boxes/search).

### Terraform

[Terraform](https://www.terraform.io/) is a tool not unlike Vagrant. It's primary purpose is to provision cloud infrastructure at scale, whereas Vagrant is used primarily for local virtual environment. Terraform uses a [domain-specific language](https://developer.hashicorp.com/terraform/language) for its configuration which is ironic as Vagrant uses Ruby (a general purpose programming language) making Vagrant's configuration system better suited for large scale provisioning and more extensible.

Terraform can interface with a vast set of providers, such as AWS, Google Cloud and libvirt.

We won't be using Terraform much on this course, but do note that the course student-facing infrastructure is provisioned using it.

### Infrastructure

There will be 3 machines in use during this exercise which can get quite complicated. Each machine has network interface controllers (NICs) that may correspond to physical or virtual devices such as an Ethernet controller or WiFi cards. Each NIC is connected to a network which some subnet spans over.

1. The local controller machine, e.g. a student laptop, where Ansible and an SSH client are installed.

   | Network | IP address/mask |
   |---------|-----------------|
   | LAN     | 10.97.0.x/18    |
   
1. The Fablab Virtual Server (VM server), where libvirt, ansible and Vagrant are installed. Provisioned by the first machine over SSH. This machines spawns the third machine.

   | Network     | IP address/mask  |
   |-------------|------------------|
   | Fablab LAN  | 10.42.0.x/24     |
   | Vagrant LAN | 192.168.121.1/24 |

1. The virtual machine we will create on the VM server with Vagrant. Also accessible over SSH from the VM server.

   | Network     | IP address/mask  |
   |-------------|------------------|
   | Vagrant LAN | 192.168.121.x/24 |

   > This machine can be accessed "directly" from the controller by using two jumphosts: the bastion and the VM server.

Here is a diagram showing how the network is set up.

![](./networking/network-kata5.drawio.svg)

Don't worry about how connections are established or packets are routed for now. Also "WAN" is not exactly a single subnet (like the other rectangles), but is used here to represent the public Internet.

## Instructions

### Step 1 — Installation

On the VM server (over SSH):

1. Check that KVM is available and install libvirt. On Ubuntu follow [these instructions](https://ubuntu.com/server/docs/virtualization-libvirt).
1. Install Vagrant.

> This doesn't need to be automated using Ansible.

### Step 2 — Vagrant project

On the VM server:

1. Create a directory, for example `kata5` for the Vagrant project, and `cd` to it.
1. Initialize the Vagrant project with the box `generic/ubuntu2204`:
   ```console
   vagrant init generic/ubuntu2204
   ```
   > Note that this image is 1.6G in size and may take quite long to download. You are allowed to use any GNU/Linux distribution that has a smaller image instead.

1. Launch the VM:
   ```console
   vagrant up --provider=libvirt
   ```
   > If you get a Permission denied error, make sure you are part of the `libvirt` group and try to relogin.

1. Connect to it over SSH and check if `/vagrant` is mounted:
   ```console
   $ vagrant ssh
   vagrant@ubuntu2204:~$ ls /vagrant
   ls: cannot access '/vagrant': No such file or directory
   ...
   ```
   > Aternatively you can connect with plain old SSH. The IP address of the VM is mentioned in the output during `vagrant up`. `default: SSH address: 192.168.121.225:22`.
   >
   > To connect to the VM directly via two jump hosts, try:
   > ```console
   > ssh -J jump@bastion.fablab.rip,localadmin@1.3.3.7 vagrant@192.168.121.225
   > ```
   > > Replace 1.3.3.7 with the VM server's Fablab LAN IP.
   >
   > The Username and Password for `generic/ubuntu2204` are `vagrant`, `vagrant`

1. Exit or login to your VM server in another terminal. Check for running Vagrant VMs using `global-status`:
   ```console
   $ vagrant global-status
   id       name    provider state   directory
   ----------------------------------------------------------------------
   95c6abc  default libvirt running /home/localadmin/kata5
   ```

1. Now in the project directory, destroy the VM
   ```console
   $ vagrant destroy
   default: Are you sure you want to destroy the 'default' VM? [y/N] y
   ```
   
### Step 3 — Shared directory

1. [Configure the VM to share](https://vagrant-libvirt.github.io/vagrant-libvirt/examples.html#synced-folders) the project directory (`./`) on the host at `/vagrant` in the guest VM. The sync type should preferably be [`virtiofs`](https://virtio-fs.gitlab.io/index.html).

   > Here are the configuration options for the libvirt provider: <https://vagrant-libvirt.github.io/vagrant-libvirt/configuration.html>
1. Connect to the VM over SSH and check that the shared folder is mounted:
   ```console
   $ vagrant ssh
   vagrant@ubuntu2204:~$ mount
   ...
   9a9bf8f41aeb89c22233ecacf3fb8f7 on /vagrant type virtiofs (rw,relatime)
   vagrant@ubuntu2204:~$ ls -tlh /vagrant
   total 4.0K
   -rw-r--r-- 1 vagrant vagrant 3.2K Mar  4 17:40 Vagrantfile
   ```

### Extra Step 4 — Provisioning the VM with Ansible

TODO ssh-copy-id etc

## Returning and feedback

Return the final `Vagrantfile` to the [Google Classroom assignment](https://classroom.google.com/c/NjAzNDI4MzQ3Mjc4/a/NjA2OTU4NTY2NTM0/details).

Finally, please fill the [kata feedback form](https://docs.google.com/forms/d/e/1FAIpQLScuGcebcVyQNcLSoYY2m4HbxXunYRfMpB-6TLIFS7inweXgYg/viewform?usp=sf_link), and then move on to the next page by pressing right.

## Commands and tools

| Command or tool                              | Summary                               |
|----------------------------------------------|---------------------------------------|
| [`ssh`](./commands.md#ssh)                   | {{#include commands.md:ssh}}          |
| [`vagrant`](./commands.md#vagrant)           | {{#include commands.md:vagrant}}      |
| [`ansible`](./commands.md#ansible)           | {{#include commands.md:ansible}}      |
| [`ansible-playbook`](./commands.md#ansible-playbook)           | {{#include commands.md:ansible-playbook}}      |

## Hints

N/A
