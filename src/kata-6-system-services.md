# Kata 6: System Services

## Learning objectives

- Understand how a Linux system boots with systemd and the philosophy behind systemd.
- Know what systemd services are.
- Know the terminology related to systemd, e.g.: service, target, process, daemon.
- Know how to deploy (and configure) new services on a system using Ansible.
- Know how to read logs emitted by services.

## Debriefing
## System target
## Commands and tools
### [`systemctl`](./commands.md#systemctl TODO)
### [`journalctl`](./commands.md#TODO)
## Hints
